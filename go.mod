module gitlab.com/AndrusGerman/mod-asisten-core

go 1.13

require (
	github.com/jinzhu/gorm v1.9.11
	github.com/otiai10/copy v1.0.2
)
