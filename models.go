package core

import "plugin"

// ModelCore model
type ModelCore struct {
	pl          *plugin.Plugin
	AsistenCore interface{}
}
